{ baseNixpkgs ? (import <nixpkgs> {}) }:

with baseNixpkgs;

let
  drvs = import ./. {};

  toJob = drvName:
    ''
      build-${drvName}:
        extends: .build
        variables:
          DRV_NAME: "${drvName}"
    '';

  ci-jobs =
    ''
      .build:
        image: nixos/nix
        stage: build
        variables:
          CACHIX_AUTH_TOKEN: unset
        tags:
          - x86_64-linux
        script:
          - nix-env -iA cachix -f https://cachix.org/api/v1/install
          - cachix use ghc
          - nix-build . --option cores $CPUS -A $DRV_NAME | cachix push ghc
    '' + lib.concatMapStringsSep "\n" toJob (lib.attrNames drvs);

in { inherit ci-jobs; }
